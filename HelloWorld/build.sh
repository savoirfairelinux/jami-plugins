#! /bin/bash
# Build the plugin for the project
set -e
export OSTYPE
ARCH=$(uname -m)
EXTRAPATH=''
# Flags:

# -p: number of processors to use
# -c: Runtime plugin cpu/gpu setting.
# -t: target platform.


if [ -z "${DAEMON}" ]; then
    DAEMON="./../../daemon"
    echo "DAEMON not provided, building with ${DAEMON}"
fi

PLUGIN_NAME="HelloWorld"
JPL_FILE_NAME="${PLUGIN_NAME}.jpl"
SO_FILE_NAME="lib${PLUGIN_NAME}.so"
DAEMON_SRC="${DAEMON}/src"
CONTRIB_PATH="${DAEMON}/contrib"
PLUGINS_LIB="../lib"
LIBS_DIR="./../contrib/Libs"
PLATFORM=$(uname)

if [ "${PLATFORM}" = "Linux" ]; then
    PLATFORM="linux-gnu"
    CONTRIB_PLATFORM_CURT=${ARCH}
    echo "Building with ${PLATFORM}"
elif [ "${PLATFORM}" = "Darwin" ]; then
    PLATFORM="darwin"
    SO_FILE_NAME="lib${PLUGIN_NAME}.dylib"
    alias nproc='sysctl -n hw.logicalcpu'
    CONTRIB_PLATFORM_CURT=${ARCH}-apple
    CONTRIB_PLATFORM_EXTRA=$(uname -r)
    echo "Building with ${PLATFORM}"
fi

while getopts t:c:p OPT; do
  case "$OPT" in
    t)
      PLATFORM="${OPTARG}"
    ;;
    c)
      PROCESSOR="${OPTARG}"
    ;;
    p)
    ;;
    \?)
      exit 1
    ;;
  esac
done

if [ "${PLATFORM}" = "linux-gnu" ] || [ "${PLATFORM}" = "redhat-linux" ]
then
    python3 ./../SDK/jplManipulation.py --preassemble --plugin=${PLUGIN_NAME}

    CONTRIB_PLATFORM=${CONTRIB_PLATFORM_CURT}-${PLATFORM}

    # Compile
    clang++ -std=c++17 -shared -fPIC \
    -Wl,-Bsymbolic,-rpath,"\${ORIGIN}" \
    -Wall -Wextra \
    -Wno-unused-variable \
    -Wno-unused-function \
    -Wno-unused-parameter \
    -I"." \
    -I"${DAEMON_SRC}" \
    -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/include" \
    -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/include/opencv4" \
    -I"${PLUGINS_LIB}" \
    ./../lib/accel.cpp \
    ./../lib/frameUtils.cpp \
    CoinCircleMediaHandler.cpp \
    CenterCircleVideoSubscriber.cpp \
    CenterCircleMediaHandler.cpp \
    CoinCircleVideoSubscriber.cpp \
    main.cpp \
    -L"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/lib/" \
    -l:libswscale.a \
    -l:libavutil.a \
    -lopencv_imgproc \
    -lopencv_core \
    -lva \
    -o "build-local/jpl/lib/${CONTRIB_PLATFORM}/${SO_FILE_NAME}"

elif [ "${PLATFORM}" = "darwin" ]
then
    python3 ./../SDK/jplManipulation.py --preassemble --plugin=${PLUGIN_NAME}

    CONTRIB_PLATFORM=${CONTRIB_PLATFORM_CURT}-${PLATFORM}

    # Compile
    clang++ -std=c++17 -shared -fPIC \
    -Wl,-no_compact_unwind -Wl,-framework,CoreFoundation \
    -Wl,-framework,Security -Wl,-framework,VideoToolbox \
    -Wl,-framework,CoreMedia -Wl,-framework,CoreVideo \
    -Wl,-framework,OpenCl -Wl,-framework,Accelerate \
    -Wl,-rpath,"\${ORIGIN}" \
    -Wall -Wextra \
    -Wno-unused-variable \
    -Wno-unused-function \
    -Wno-unused-parameter \
    -I"." \
    -I"${DAEMON_SRC}" \
    -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}${CONTRIB_PLATFORM_EXTRA}/include" \
    -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}${CONTRIB_PLATFORM_EXTRA}/include/opencv4" \
    -I"${PLUGINS_LIB}" \
    ./../lib/accel.cpp \
    ./../lib/frameUtils.cpp \
    CoinCircleMediaHandler.cpp \
    CenterCircleVideoSubscriber.cpp \
    CenterCircleMediaHandler.cpp \
    CoinCircleVideoSubscriber.cpp \
    main.cpp \
    -L"${CONTRIB_PATH}/${CONTRIB_PLATFORM}${CONTRIB_PLATFORM_EXTRA}/lib/" \
    -lswscale \
    -lavutil \
    -lopencv_imgproc \
    -lopencv_core \
    -lvpx -lx264 -lbz2 -liconv -lz -lspeex -lopus \
    -o "build-local/jpl/lib/${CONTRIB_PLATFORM}/${SO_FILE_NAME}"

    install_name_tool -id "@loader_path/${SO_FILE_NAME}" "build-local/jpl/lib/${CONTRIB_PLATFORM}/${SO_FILE_NAME}"

    if [ -n "${APPLE_SIGN_CERTIFICATE}" ]; then
      codesign --force --verify --timestamp -o runtime --sign "${APPLE_SIGN_CERTIFICATE}"  "build-local/jpl/lib/${CONTRIB_PLATFORM}/${SO_FILE_NAME}"
      ditto -c -k --rsrc "build-local/jpl/lib/${CONTRIB_PLATFORM}/${SO_FILE_NAME}" "build-local/${SO_FILE_NAME}.zip"
      LIBRARYNAME=${PLUGIN_NAME} sh ./../notarize.sh
      cd ..
      ditto -x -k "build-local/${SO_FILE_NAME}.zip" "build-local/notarized"
      cp "build-local/notarized/${SO_FILE_NAME}" "build-local/jpl/lib/${CONTRIB_PLATFORM}/${SO_FILE_NAME}"
    fi

elif [ "${PLATFORM}" = "android" ]
then
    python3 ./../SDK/jplManipulation.py --preassemble --plugin=${PLUGIN_NAME} --distribution=${PLATFORM}

    if [ -z "$ANDROID_NDK" ]; then
            ANDROID_NDK="/home/${USER}/Android/Sdk/ndk/21.1.6352462"
        echo "ANDROID_NDK not provided, building with ${ANDROID_NDK}"
    fi

    #=========================================================
    #    Check if the ANDROID_ABI was provided
    #    if not, set default
    #=========================================================
    if [ -z "$ANDROID_ABI" ]; then
        ANDROID_ABI="armeabi-v7a arm64-v8a x86_64"
        echo "ANDROID_ABI not provided, building for ${ANDROID_ABI}"
    fi

    buildlib() {
        echo "$CURRENT_ABI"

        #=========================================================
        #    ANDROID TOOLS
        #=========================================================
        export HOST_TAG=linux-x86_64
        export TOOLCHAIN=$ANDROID_NDK/toolchains/llvm/prebuilt/$HOST_TAG
        export AR=$TOOLCHAIN/bin/llvm-ar
        export AS=$TOOLCHAIN/bin/llvm-as
        export LD=$TOOLCHAIN/bin/ld
        export RANLIB=$TOOLCHAIN/bin/llvm-ranlib
        export STRIP=$TOOLCHAIN/bin/llvm-strip
        export ANDROID_SYSROOT=$TOOLCHAIN/sysroot

        if [ "$CURRENT_ABI" = armeabi-v7a ]
        then
        export CC=$TOOLCHAIN/bin/armv7a-linux-androideabi21-clang
        export CXX=$TOOLCHAIN/bin/armv7a-linux-androideabi21-clang++

        elif [ "$CURRENT_ABI" = arm64-v8a ]
        then
        export CC=$TOOLCHAIN/bin/aarch64-linux-android21-clang
        export CXX=$TOOLCHAIN/bin/aarch64-linux-android21-clang++

        elif [ "$CURRENT_ABI" = x86_64 ]
        then
        export CC=$TOOLCHAIN/bin/x86_64-linux-android21-clang
        export CXX=$TOOLCHAIN/bin/x86_64-linux-android21-clang++

        else
        echo "ABI NOT OK" >&2
        exit 1
        fi

        #=========================================================
        #    CONTRIBS
        #=========================================================
        if [ "$CURRENT_ABI" = armeabi-v7a ]
        then
        CONTRIB_PLATFORM=arm-linux-androideabi

        elif [ "$CURRENT_ABI" = arm64-v8a ]
        then
        CONTRIB_PLATFORM=aarch64-linux-android

        elif [ "$CURRENT_ABI" = x86_64 ]
        then
        CONTRIB_PLATFORM=x86_64-linux-android
        fi

        #=========================================================
        #    Compile the plugin
        #=========================================================

        # Create so destination folder
        $CXX --std=c++17 -O3 -fPIC \
        -Wl,-Bsymbolic,-rpath,"\${ORIGIN}" \
        -shared \
        -Wall -Wextra \
        -Wno-unused-parameter \
        -I"." \
        -I"${DAEMON_SRC}" \
        -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/include" \
        -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/include/opencv4" \
        -I"${PLUGINS_LIB}" \
        ./../lib/accel.cpp \
        ./../lib/frameUtils.cpp \
        CoinCircleMediaHandler.cpp \
        CenterCircleVideoSubscriber.cpp \
        CenterCircleMediaHandler.cpp \
        CoinCircleVideoSubscriber.cpp \
        main.cpp \
        -L"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/lib/" \
        -lswscale \
        -lavutil \
        -lopencv_imgproc \
        -lopencv_core \
        -llog -lz \
        --sysroot=$ANDROID_SYSROOT \
        -o "build-local/jpl/lib/$CURRENT_ABI/${SO_FILE_NAME}"
    }

    # Build the so
    for i in ${ANDROID_ABI}; do
        CURRENT_ABI=$i
        buildlib
    done
fi

python3 ./../SDK/jplManipulation.py --assemble --plugin=${PLUGIN_NAME} --distribution=${PLATFORM} --extraPath=${EXTRAPATH}
