#!/usr/bin/env python3
#
# Copyright (C) 2020-2021 Savoir-faire Linux Inc.
#
# Author: Aline Gondim Santos <aline.gondimsantos@savoirfairelinux.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Creates packaging targets for a distribution and architecture.
# This helps reduce the length of the top Makefile.
#

import os
import glob
import platform

from sdkConstants import *
from skeletonSrcProfile import SkeletonSourceFiles


def initializeClasses(action):
    setSkeletonSourceFiles(action)
    return bool(globalSkeletonSourceFiles.pluginName)


globalSkeletonSourceFiles = ''


def setSkeletonSourceFiles(action):
    global globalSkeletonSourceFiles
    globalSkeletonSourceFiles = SkeletonSourceFiles(action)


def getSkeletonSourceFiles():
    return globalSkeletonSourceFiles
