#! /bin/bash
# Build the plugin for the project
set -e
export OSTYPE
ARCH=$(arch)
EXTRAPATH=''
# Flags:

# -p: number of processors to use
# -c: Runtime plugin cpu/gpu setting.
# -t: target platform.


if [ -z "${DAEMON}" ]; then
    DAEMON="./../../daemon"
    echo "DAEMON not provided, building with ${DAEMON}"
fi

PLUGIN_NAME="PLUGINNAME"
JPL_FILE_NAME="${PLUGIN_NAME}.jpl"
SO_FILE_NAME="lib${PLUGIN_NAME}.so"
DAEMON_SRC="${DAEMON}/src"
CONTRIB_PATH="${DAEMON}/contrib"
PLUGINS_LIB="../lib"
LIBS_DIR="./../contrib/Libs"

if [ -z "${PLATFORM}" ]; then
    PLATFORM="linux-gnu"
    echo "PLATFORM not provided, building with ${PLATFORM}"
    echo "Other options: redhat-linux"
fi

while getopts t:c:p OPT; do
  case "$OPT" in
    t)
      PLATFORM="${OPTARG}"
    ;;
    c)
      PROCESSOR="${OPTARG}"
    ;;
    p)
    ;;
    \?)
      exit 1
    ;;
  esac
done

if [ "${PLATFORM}" = "linux-gnu" ] || [ "${PLATFORM}" = "redhat-linux" ]
then
    python3 ./../SDK/jplManipulation.py --preassemble --plugin=${PLUGIN_NAME}

    CONTRIB_PLATFORM_CURT=${ARCH}
    CONTRIB_PLATFORM=${CONTRIB_PLATFORM_CURT}-${PLATFORM}

    # Compile
    clang++ -std=c++17 -shared -fPIC \
    -Wl,-Bsymbolic,-rpath,"\${ORIGIN}" \
    -Wall -Wextra \
    -Wno-unused-variable \
    -Wno-unused-function \
    -Wno-unused-parameter \
    -I"." \
    -I"${DAEMON_SRC}" \
    -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/include" \
    -I"${PLUGINS_LIB}" \
    ---FFMPEGCPP \
    ---CPPFILENAME \
    ----L"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/lib/" \
    ----FFMPEGLIBS \
    ---FFMPEGEXTRA-lva \
    ----o "build-local/jpl/lib/${CONTRIB_PLATFORM_CURT}-linux-gnu/${SO_FILE_NAME}"

elif [ "${PLATFORM}" = "android" ]
then
    python3 ./../SDK/jplManipulation.py --preassemble --plugin=${PLUGIN_NAME} --distribution=${PLATFORM}

    if [ -z "$ANDROID_NDK" ]; then
            ANDROID_NDK="/home/${USER}/Android/Sdk/ndk/21.1.6352462"
        echo "ANDROID_NDK not provided, building with ${ANDROID_NDK}"
    fi

    #=========================================================
    #    Check if the ANDROID_ABI was provided
    #    if not, set default
    #=========================================================
    if [ -z "$ANDROID_ABI" ]; then
        ANDROID_ABI="armeabi-v7a arm64-v8a x86_64"
        echo "ANDROID_ABI not provided, building for ${ANDROID_ABI}"
    fi

    buildlib() {
        echo "$CURRENT_ABI"

        #=========================================================
        #    ANDROID TOOLS
        #=========================================================
        export HOST_TAG=linux-x86_64
        export TOOLCHAIN=$ANDROID_NDK/toolchains/llvm/prebuilt/$HOST_TAG
        export AR=$TOOLCHAIN/bin/llvm-ar
        export AS=$TOOLCHAIN/bin/llvm-as
        export LD=$TOOLCHAIN/bin/ld
        export RANLIB=$TOOLCHAIN/bin/llvm-ranlib
        export STRIP=$TOOLCHAIN/bin/llvm-strip
        export ANDROID_SYSROOT=$TOOLCHAIN/sysroot

        if [ "$CURRENT_ABI" = armeabi-v7a ]
        then
        export CC=$TOOLCHAIN/bin/armv7a-linux-androideabi21-clang
        export CXX=$TOOLCHAIN/bin/armv7a-linux-androideabi21-clang++

        elif [ "$CURRENT_ABI" = arm64-v8a ]
        then
        export CC=$TOOLCHAIN/bin/aarch64-linux-android21-clang
        export CXX=$TOOLCHAIN/bin/aarch64-linux-android21-clang++

        elif [ "$CURRENT_ABI" = x86_64 ]
        then
        export CC=$TOOLCHAIN/bin/x86_64-linux-android21-clang
        export CXX=$TOOLCHAIN/bin/x86_64-linux-android21-clang++

        else
        echo "ABI NOT OK" >&2
        exit 1
        fi

        #=========================================================
        #    CONTRIBS
        #=========================================================
        if [ "$CURRENT_ABI" = armeabi-v7a ]
        then
        CONTRIB_PLATFORM=arm-linux-androideabi

        elif [ "$CURRENT_ABI" = arm64-v8a ]
        then
        CONTRIB_PLATFORM=aarch64-linux-android

        elif [ "$CURRENT_ABI" = x86_64 ]
        then
        CONTRIB_PLATFORM=x86_64-linux-android
        fi

        #=========================================================
        #    Compile the plugin
        #=========================================================

        # Create so destination folder
        $CXX --std=c++17 -O3 -fPIC \
        -Wl,-Bsymbolic,-rpath,"\${ORIGIN}" \
        -shared \
        -Wall -Wextra \
        -Wno-unused-parameter \
        -I"." \
        -I"${DAEMON_SRC}" \
        -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/include" \
        -I"${PLUGINS_LIB}" \
        ---FFMPEGCPP \
        ---CPPFILENAME \
        ----L"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/lib/" \
        ----FFMPEGLIBS \
        ----llog -lz \
        --sysroot=$ANDROID_SYSROOT \
        -o "build-local/jpl/lib/$CURRENT_ABI/${SO_FILE_NAME}"
    }

    # Build the so
    for i in ${ANDROID_ABI}; do
        CURRENT_ABI=$i
        buildlib
    done
fi

python3 ./../SDK/jplManipulation.py --assemble --plugin=${PLUGIN_NAME} --distribution=${PLATFORM} --extraPath=${EXTRAPATH}
