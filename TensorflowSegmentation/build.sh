#! /bin/bash
# Build the plugin for the project
set -e
export OSTYPE
ARCH=$(arch)
EXTRAPATH=''
# Flags:

  # -p: number of processors to use
  # -c: Runtime plugin cpu/gpu setting.
  # -t: target platform.


if [ -z "${DAEMON}" ]; then
    DAEMON="./../../daemon"
    echo "DAEMON not provided, building with ${DAEMON}"
fi

PLUGIN_NAME="TensorflowSegmentation"
JPL_FILE_NAME="${PLUGIN_NAME}.jpl"
SO_FILE_NAME="lib${PLUGIN_NAME}.so"
DAEMON_SRC="${DAEMON}/src"
CONTRIB_PATH="${DAEMON}/contrib"
PLUGINS_LIB="../lib"
LIBS_DIR="./../contrib/Libs"

if [ -z "${TF_LIBS_DIR}" ]; then
    TF_LIBS_DIR="./../../../Libs"
fi
echo "Building with ${TF_LIBS_DIR}"

PLATFORM="linux-gnu"
PROCESSOR='GPU'

while getopts t:c:p OPT; do
  case "$OPT" in
    t)
      PLATFORM="${OPTARG}"
      if [ -z "${TF}" ]; then
          if [ "$PLATFORM" = 'linux-gnu' ]; then
              TF="_tensorflow_cc"
          elif [ "$PLATFORM" = 'android' ]; then
              TF="_tensorflowLite"
          fi
      fi
    ;;
    c)
      PROCESSOR="${OPTARG}"
    ;;
    p)
    ;;
    \?)
      exit 1
    ;;
  esac
done


if [ -z "${TF}" ]; then
    TF="_tensorflow_cc"
fi
echo "Building with ${TF}"

if [[ "${TF}" = "_tensorflow_cc" ]] && [[ "${PLATFORM}" = "linux-gnu" ]]
then
    if [ -z "$CUDALIBS" ]; then
        echo "CUDALIBS must point to CUDA 10.1!"
        exit
    fi
    if [ -z "$CUDNN" ]; then
        echo "CUDNN must point to libcudnn.so 7!"
        exit
    fi

    echo "Building for ${PROCESSOR}"

    python3 ./../SDK/jplManipulation.py --preassemble --plugin=${PLUGIN_NAME}

    CONTRIB_PLATFORM_CURT=${ARCH}
    CONTRIB_PLATFORM=${CONTRIB_PLATFORM_CURT}-${PLATFORM}
    EXTRAPATH=${TF}

    # Compile
    clang++ -std=c++17 -shared -fPIC \
    -Wl,-Bsymbolic,-rpath,"\${ORIGIN}" \
    -Wall -Wextra \
    -Wno-unused-variable \
    -Wno-unused-function \
    -Wno-unused-parameter \
    -D"${PROCESSOR}" \
    -I"." \
    -I"${DAEMON_SRC}" \
    -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/include" \
    -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/include/opencv4" \
    -I"${LIBS_DIR}/${TF}/include" \
    -I"${LIBS_DIR}/${TF}/include/third_party/eigen3" \
    -I"${PLUGINS_LIB}" \
    ./../lib/accel.cpp \
    ./../lib/frameUtils.cpp \
    main.cpp \
    videoSubscriber.cpp \
    pluginProcessor.cpp \
    pluginMediaHandler.cpp \
    TFInference.cpp \
    pluginInference.cpp \
    pluginParameters.cpp \
    -L"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/lib/" \
    -L"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/lib/opencv4/3rdparty/" \
    -L"${TF_LIBS_DIR}/${TF}/lib/${CONTRIB_PLATFORM}-gpu61/" \
    -l:libswscale.a \
    -l:libavutil.a \
    -lopencv_imgcodecs \
    -lopencv_imgproc \
    -lopencv_core \
    -llibpng \
    -lva \
    -ltensorflow_cc \
    -o "build-local/jpl/lib/${CONTRIB_PLATFORM}/${SO_FILE_NAME}"

    cp "${TF_LIBS_DIR}/${TF}/lib/${CONTRIB_PLATFORM}-gpu61/libtensorflow_cc.so" "build-local/jpl/lib/$CONTRIB_PLATFORM/libtensorflow_cc.so.2"
    cp "${CUDALIBS}/libcudart.so" "build-local/jpl/lib/$CONTRIB_PLATFORM/libcudart.so.10.0"
    cp "${CUDNN}/libcublas.so.10" "build-local/jpl/lib/$CONTRIB_PLATFORM/libcublas.so.10.0"
    cp "${CUDALIBS}/libcufft.so.10" "build-local/jpl/lib/$CONTRIB_PLATFORM/libcufft.so.10.0"
    cp "${CUDALIBS}/libcurand.so.10" "build-local/jpl/lib/$CONTRIB_PLATFORM/libcurand.so.10.0"
    cp "${CUDALIBS}/libcusolver.so.10" "build-local/jpl/lib/$CONTRIB_PLATFORM/libcusolver.so.10.0"
    cp "${CUDALIBS}/libcusparse.so.10" "build-local/jpl/lib/$CONTRIB_PLATFORM/libcusparse.so.10.0"
    cp "${CUDNN}/libcudnn.so.7" "build-local/jpl/lib/$CONTRIB_PLATFORM"

    pwd
    mkdir ./build-local/jpl/data/models
    cp ./modelsSRC/mModel-resnet50float.pb ./build-local/jpl/data/models/mModel.pb
    cp ./preferences-tfcc.json ./build-local/jpl/data/preferences.json
elif [ "${TF}" = "_tensorflowLite" ]
then
    if [ "${PLATFORM}" = "linux-gnu" ]
    then
        python3 ./../SDK/jplManipulation.py --preassemble --plugin=${PLUGIN_NAME}

        CONTRIB_PLATFORM_CURT=${ARCH}
        CONTRIB_PLATFORM=${CONTRIB_PLATFORM_CURT}-${PLATFORM}
        EXTRAPATH="${TF}"

        # Compile
        clang++ -std=c++17 -shared -fPIC \
        -Wl,-Bsymbolic,-rpath,"\${ORIGIN}" \
        -Wall -Wextra \
        -Wno-unused-variable \
        -Wno-unused-function \
        -Wno-unused-parameter \
        -DTFLITE \
        -I"." \
        -I"${DAEMON_SRC}" \
        -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/include" \
        -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/include/opencv4" \
        -I"${LIBS_DIR}/${TF}/include/flatbuffers" \
        -I"${LIBS_DIR}/${TF}/include" \
        -I"${PLUGINS_LIB}" \
        ./../lib/accel.cpp \
        ./../lib/frameUtils.cpp \
        videoSubscriber.cpp \
        pluginProcessor.cpp \
        pluginMediaHandler.cpp \
        TFInference.cpp \
        pluginInference.cpp \
        pluginParameters.cpp \
        main.cpp \
        -L"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/lib/" \
        -L"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/lib/opencv4/3rdparty/" \
        -L"${TF_LIBS_DIR}/${TF}/lib/${CONTRIB_PLATFORM}/" \
        -l:libswscale.a \
        -l:libavutil.a \
        -lopencv_imgcodecs \
        -lopencv_imgproc \
        -lopencv_core \
        -ltensorflowlite \
        -llibpng \
        -lva \
        -o "build-local/jpl/lib/${CONTRIB_PLATFORM}/${SO_FILE_NAME}"

        cp "${TF_LIBS_DIR}/${TF}/lib/${CONTRIB_PLATFORM}/libtensorflowlite.so" "build-local/jpl/lib/$CONTRIB_PLATFORM"

    elif [ "${PLATFORM}" = "android" ]
    then
        python3 ./../SDK/jplManipulation.py --preassemble --plugin=${PLUGIN_NAME} --distribution=${PLATFORM}

        if [ -z "$ANDROID_NDK" ]; then
             ANDROID_NDK="/home/${USER}/Android/Sdk/ndk/21.1.6352462"
            echo "ANDROID_NDK not provided, building with ${ANDROID_NDK}"
        fi

        #=========================================================
        #    Check if the ANDROID_ABI was provided
        #    if not, set default
        #=========================================================
        if [ -z "$ANDROID_ABI" ]; then
            ANDROID_ABI="armeabi-v7a arm64-v8a"
            echo "ANDROID_ABI not provided, building for ${ANDROID_ABI}"
        fi

        buildlib() {
            echo "$CURRENT_ABI"

            #=========================================================
            #    ANDROID TOOLS
            #=========================================================
            export HOST_TAG=linux-x86_64
            export TOOLCHAIN=$ANDROID_NDK/toolchains/llvm/prebuilt/$HOST_TAG
            export AR=$TOOLCHAIN/bin/llvm-ar
            export AS=$TOOLCHAIN/bin/llvm-as
            export LD=$TOOLCHAIN/bin/ld
            export RANLIB=$TOOLCHAIN/bin/llvm-ranlib
            export STRIP=$TOOLCHAIN/bin/llvm-strip
            export ANDROID_SYSROOT=$TOOLCHAIN/sysroot

            if [ "$CURRENT_ABI" = armeabi-v7a ]
            then
            export CC=$TOOLCHAIN/bin/armv7a-linux-androideabi21-clang
            export CXX=$TOOLCHAIN/bin/armv7a-linux-androideabi21-clang++

            elif [ "$CURRENT_ABI" = arm64-v8a ]
            then
            export CC=$TOOLCHAIN/bin/aarch64-linux-android21-clang
            export CXX=$TOOLCHAIN/bin/aarch64-linux-android21-clang++

            elif [ "$CURRENT_ABI" = x86_64 ]
            then
            export CC=$TOOLCHAIN/bin/x86_64-linux-android21-clang
            export CXX=$TOOLCHAIN/bin/x86_64-linux-android21-clang++

            else
            echo "ABI NOT OK" >&2
            exit 1
            fi

            #=========================================================
            #    CONTRIBS
            #=========================================================
            if [ "$CURRENT_ABI" = armeabi-v7a ]
            then
            CONTRIB_PLATFORM=arm-linux-androideabi

            elif [ "$CURRENT_ABI" = arm64-v8a ]
            then
            CONTRIB_PLATFORM=aarch64-linux-android

            elif [ "$CURRENT_ABI" = x86_64 ]
            then
            CONTRIB_PLATFORM=x86_64-linux-android
            fi

            #NDK SOURCES FOR cpufeatures
            NDK_SOURCES=${ANDROID_NDK}/sources/android

            #=========================================================
            #    Compile CPU FEATURES, NEEDED FOR OPENCV
            #=========================================================
            $CC -c "$NDK_SOURCES/cpufeatures/cpu-features.c" -o cpu-features.o -o cpu-features.o --sysroot=$ANDROID_SYSROOT

            #=========================================================
            #    Compile the plugin
            #=========================================================

            # Create so destination folder
            $CXX --std=c++17 -O3 -fPIC \
            -Wl,-Bsymbolic,-rpath,"\${ORIGIN}" \
            -shared \
            -Wall -Wextra \
            -Wno-unused-parameter \
            -DTFLITE \
            -I"." \
            -I"${DAEMON_SRC}" \
            -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/include" \
            -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/include/opencv4" \
            -I"${LIBS_DIR}/${TF}/include/flatbuffers" \
            -I"${LIBS_DIR}/${TF}/include" \
            -I"${PLUGINS_LIB}" \
            ./../lib/accel.cpp \
            ./../lib/frameUtils.cpp \
            main.cpp \
            videoSubscriber.cpp \
            pluginProcessor.cpp \
            pluginMediaHandler.cpp \
            TFInference.cpp \
            pluginInference.cpp \
            pluginParameters.cpp \
            cpu-features.o \
            -L"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/lib/" \
            -L"${TF_LIBS_DIR}/${TF}/lib/${CURRENT_ABI}/" \
            -lswscale \
            -lavutil \
            -lopencv_imgcodecs \
            -lopencv_imgproc \
            -lopencv_core \
            -llibpng \
            -ltensorflowlite \
            -llog -lz \
            --sysroot=$ANDROID_SYSROOT \
            -o "build-local/jpl/lib/$CURRENT_ABI/${SO_FILE_NAME}"

            cp "${TF_LIBS_DIR}/${TF}/lib/${CURRENT_ABI}/libtensorflowlite.so" "build-local/jpl/lib/$CURRENT_ABI"
            rm cpu-features.o
        }

        # Build the so
        for i in ${ANDROID_ABI}; do
            CURRENT_ABI=$i
            buildlib
        done
    fi

    mkdir ./build-local/jpl/data/models
    cp ./modelsSRC/mobilenet_v2_deeplab_v3_256_myquant.tflite ./build-local/jpl/data/models/mModel.tflite
    cp ./preferences-tflite.json ./build-local/jpl/data/preferences.json
fi

python3 ./../SDK/jplManipulation.py --assemble --plugin=${PLUGIN_NAME} --distribution=${PLATFORM} --extraPath=${EXTRAPATH}
