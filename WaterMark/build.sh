#! /bin/bash
# Build the plugin for the project
set -e
export OSTYPE
ARCH=$(uname -m)
EXTRAPATH=''
# Flags:

# -p: number of processors to use
# -c: Runtime plugin cpu/gpu setting.
# -t: target platform.
# -d: debug program.


if [ -z "${DAEMON}" ]; then
    DAEMON="./../../daemon"
    echo "DAEMON not provided, building with ${DAEMON}"
fi

PLUGIN_NAME="WaterMark"
JPL_FILE_NAME="${PLUGIN_NAME}.jpl"
SO_FILE_NAME="lib${PLUGIN_NAME}.so"
DAEMON_SRC="${DAEMON}/src"
CONTRIB_PATH="${DAEMON}/contrib"
PLUGINS_LIB="../lib"
LIBS_DIR="./../contrib/Libs"
PLATFORM=$(uname)

if [ "${PLATFORM}" = "Linux" ]; then
    PLATFORM="linux-gnu"
    CONTRIB_PLATFORM_CURT=${ARCH}
    echo "Building with ${PLATFORM}"
elif [ "${PLATFORM}" = "Darwin" ]; then
    PLATFORM="darwin"
    SO_FILE_NAME="lib${PLUGIN_NAME}.dylib"
    alias nproc="sysctl -n hw.logicalcpu"
    CONTRIB_PLATFORM_CURT=${ARCH}-apple
    CONTRIB_PLATFORM_EXTRA=$(uname -r)
    echo "Building with ${PLATFORM}"
fi

while getopts t:c:p:d OPT; do
  case "$OPT" in
    d)
      DEBUG=true
      export __DEBUG__=true
    ;;
    t)
      PLATFORM="${OPTARG}"
    ;;
    c)
      PROCESSOR="${OPTARG}"
    ;;
    p)
    ;;
    \?)
      exit 1
    ;;
  esac
done

cp -r ffmpeg ${CONTRIB_PATH}/src/
cp -r ../contrib/rav1e ${CONTRIB_PATH}/src/

if [ "${PLATFORM}" = "linux-gnu" ] || [ "${PLATFORM}" = "redhat-linux" ]
then
    if [ -f "${CONTRIB_PATH}/native/.ffmpeg" ]; then
        rm "${CONTRIB_PATH}/native/.ffmpeg"
    fi
    WORKPATH=$(pwd)
    cd "${CONTRIB_PATH}/native/"
    make .ffmpeg -j$(nproc)
    rm .ffmpeg
    cd ${WORKPATH}

    CONTRIB_PLATFORM=${CONTRIB_PLATFORM_CURT}-${PLATFORM}

    if [ ${DEBUG} ]; then
      OUTPUT="${PLUGIN_NAME}"
      CLANG_OPTS="-g -fsanitize=address"
      EXTRA_DEBUG_LIBRARIES="-lyaml-cpp -lvdpau -lX11 -lva-drm -lva-x11 -lrav1e"
      EXTRA_DEFINES="-D__DEBUG__"
    else
      python3 ./../SDK/jplManipulation.py --preassemble --plugin=${PLUGIN_NAME}
      CLANG_OPTS="-O3 -shared"
      OUTPUT="build-local/jpl/lib/${CONTRIB_PLATFORM}/${SO_FILE_NAME}"
    fi

    # Compile
    clang++ -std=c++17 -fPIC ${CLANG_OPTS} \
    -Wl,-Bsymbolic,-rpath,"\${ORIGIN}" \
    -Wall -Wextra \
    -Wno-unused-parameter \
    ${EXTRA_DEFINES} \
    -I"." \
    -I"${DAEMON_SRC}" \
    -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/include" \
    -I"${PLUGINS_LIB}" \
    ./../lib/common.cpp \
    ./../lib/accel.cpp \
    ./../lib/frameUtils.cpp \
    ./../lib/frameFilter.cpp \
    WatermarkVideoSubscriber.cpp \
    main.cpp \
    WatermarkMediaHandler.cpp \
    PluginPreferenceHandler.cpp \
    -L"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/lib/" \
    -l:libavfilter.a \
    -l:libswscale.a \
    -l:libavformat.a \
    -l:libavcodec.a \
    -l:libavutil.a \
    -l:libvpx.a \
    -l:libx264.a \
    -lfreetype \
    -lvpx \
    -lx264 \
    -lspeex \
    -lopus \
    -lz \
    -lva \
    ${EXTRA_DEBUG_LIBRARIES} \
    -o "${OUTPUT}"

elif [ "${PLATFORM}" = "darwin" ]
then
    if [ -f "${CONTRIB_PATH}/native/.ffmpeg" ]; then
        rm "${CONTRIB_PATH}/native/.ffmpeg"
    fi
    WORKPATH=$(pwd)
    cd "${CONTRIB_PATH}/native"
    make .ffmpeg -j$(nproc)
    rm .ffmpeg
    cd ${WORKPATH}

    CONTRIB_PLATFORM=${CONTRIB_PLATFORM_CURT}-${PLATFORM}

    if [ ${DEBUG} ]; then
      OUTPUT="${PLUGIN_NAME}"
      CLANG_OPTS="-g -fsanitize=address"
      EXTRA_DEBUG_LIBRARIES="-lyaml-cpp -lrav1e"
      EXTRA_DEFINES="-D__DEBUG__"
    else
      python3 ./../SDK/jplManipulation.py --preassemble --plugin=${PLUGIN_NAME}
      CLANG_OPTS="-O3 -shared"
      OUTPUT="build-local/jpl/lib/${CONTRIB_PLATFORM}/${SO_FILE_NAME}"
    fi

    # Compile
    clang++ -std=c++17 -fPIC ${CLANG_OPTS} \
    -Wl,-no_compact_unwind -Wl,-framework,CoreFoundation \
    -Wl,-framework,Security -Wl,-framework,VideoToolbox \
    -Wl,-framework,CoreMedia -Wl,-framework,CoreVideo \
    -Wl,-rpath,"\${ORIGIN}" \
    -Wall -Wextra \
    -Wno-unused-parameter \
    ${EXTRA_DEFINES} \
    -I"." \
    -I"${DAEMON_SRC}" \
    -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}${CONTRIB_PLATFORM_EXTRA}/include" \
    -I"${PLUGINS_LIB}" \
    ./../lib/common.cpp \
    ./../lib/accel.cpp \
    ./../lib/frameUtils.cpp \
    ./../lib/frameFilter.cpp \
    WatermarkVideoSubscriber.cpp \
    main.cpp \
    WatermarkMediaHandler.cpp \
    PluginPreferenceHandler.cpp \
    -L"${CONTRIB_PATH}/${CONTRIB_PLATFORM}${CONTRIB_PLATFORM_EXTRA}/lib/" \
    -lavfilter \
    -lswscale \
    -lavformat \
    -lavcodec \
    -lavutil \
    -lvpx -lx264 -lbz2 -liconv -lz \
    -lspeex \
    -lopus \
    "/usr/local/opt/libpng/lib/libpng.a" \
    "/usr/local/opt/freetype/lib/libfreetype.a" \
    ${EXTRA_DEBUG_LIBRARIES} \
    -o "${OUTPUT}"

    if [ ! ${DEBUG} ]; then
        install_name_tool -id "@loader_path/${PLUGIN_NAME}" "${OUTPUT}"
    else
        install_name_tool -id "@loader_path/${SO_FILE_NAME}" "${OUTPUT}"
    fi

    if [ -n "${APPLE_SIGN_CERTIFICATE}" ]; then
      codesign --force --verify --timestamp -o runtime --sign "${APPLE_SIGN_CERTIFICATE}"  "build-local/jpl/lib/${CONTRIB_PLATFORM}/${SO_FILE_NAME}"
      ditto -c -k --rsrc "build-local/jpl/lib/${CONTRIB_PLATFORM}/${SO_FILE_NAME}" "build-local/${SO_FILE_NAME}.zip"
      LIBRARYNAME=${SO_FILE_NAME} sh ./../notarize.sh
      ditto -x -k "build-local/${SO_FILE_NAME}.zip" "build-local/notarized"
      cp "build-local/notarized/${SO_FILE_NAME}" "build-local/jpl/lib/${CONTRIB_PLATFORM}/${SO_FILE_NAME}"
    fi

elif [ "${PLATFORM}" = "android" ]
then
    python3 ./../SDK/jplManipulation.py --preassemble --plugin=${PLUGIN_NAME} --distribution=${PLATFORM}

    if [ -z "$ANDROID_NDK" ]; then
            ANDROID_NDK="/home/${USER}/Android/Sdk/ndk/21.1.6352462"
        echo "ANDROID_NDK not provided, building with ${ANDROID_NDK}"
    fi

    #=========================================================
    #    Check if the ANDROID_ABI was provided
    #    if not, set default
    #=========================================================
    if [ -z "$ANDROID_ABI" ]; then
        ANDROID_ABI="armeabi-v7a arm64-v8a x86_64"
        echo "ANDROID_ABI not provided, building for ${ANDROID_ABI}"
    fi

    buildlib() {
        echo "$CURRENT_ABI"

        #=========================================================
        #    ANDROID TOOLS
        #=========================================================
        export HOST_TAG=linux-x86_64
        export TOOLCHAIN=$ANDROID_NDK/toolchains/llvm/prebuilt/$HOST_TAG
        export AR=$TOOLCHAIN/bin/llvm-ar
        export AS=$TOOLCHAIN/bin/llvm-as
        export LD=$TOOLCHAIN/bin/ld
        export RANLIB=$TOOLCHAIN/bin/llvm-ranlib
        export STRIP=$TOOLCHAIN/bin/llvm-strip
        export ANDROID_SYSROOT=$TOOLCHAIN/sysroot

        if [ "$CURRENT_ABI" = armeabi-v7a ]
        then
        export CC=$TOOLCHAIN/bin/armv7a-linux-androideabi21-clang
        export CXX=$TOOLCHAIN/bin/armv7a-linux-androideabi21-clang++

        elif [ "$CURRENT_ABI" = arm64-v8a ]
        then
        export CC=$TOOLCHAIN/bin/aarch64-linux-android21-clang
        export CXX=$TOOLCHAIN/bin/aarch64-linux-android21-clang++

        elif [ "$CURRENT_ABI" = x86_64 ]
        then
        export CC=$TOOLCHAIN/bin/x86_64-linux-android21-clang
        export CXX=$TOOLCHAIN/bin/x86_64-linux-android21-clang++

        else
        echo "ABI NOT OK" >&2
        exit 1
        fi

        #=========================================================
        #    CONTRIBS
        #=========================================================
        if [ "$CURRENT_ABI" = armeabi-v7a ]
        then
        CONTRIB_PLATFORM=arm-linux-androideabi

        elif [ "$CURRENT_ABI" = arm64-v8a ]
        then
        CONTRIB_PLATFORM=aarch64-linux-android

        elif [ "$CURRENT_ABI" = x86_64 ]
        then
        CONTRIB_PLATFORM=x86_64-linux-android
        fi

        if [ -f "${CONTRIB_PATH}/native-${CONTRIB_PLATFORM}/.ffmpeg" ]; then
            rm "${CONTRIB_PATH}/native-${CONTRIB_PLATFORM}/.ffmpeg"
        fi

        WORKPATH=$(pwd)
        cd "${CONTRIB_PATH}/native-${CONTRIB_PLATFORM}/"
        make .ffmpeg -j$(nproc)
        rm .ffmpeg
        cd ${WORKPATH}

        #=========================================================
        #    Compile the plugin
        #=========================================================

        # Create so destination folder
        $CXX --std=c++17 -O3 -fPIC \
        -Wl,-Bsymbolic,-rpath,"\${ORIGIN}" \
        -shared \
        -Wall -Wextra \
        -Wno-unused-parameter \
        -I"." \
        -I"${DAEMON_SRC}" \
        -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/include" \
        -I"${PLUGINS_LIB}" \
        ./../lib/common.cpp \
        ./../lib/accel.cpp \
        ./../lib/frameUtils.cpp \
        ./../lib/frameFilter.cpp \
        WatermarkVideoSubscriber.cpp \
        main.cpp \
        WatermarkMediaHandler.cpp \
        PluginPreferenceHandler.cpp \
        -L"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/lib/" \
        -lavfilter \
        -lswscale \
        -lavformat \
        -lavcodec \
        -lavutil \
        -lvpx \
        -lx264 \
        -lspeex \
        -lopus \
        -liconv \
        -l:libfreetype.a \
        -llog -lz \
        --sysroot=$ANDROID_SYSROOT \
        -o "build-local/jpl/lib/$CURRENT_ABI/${SO_FILE_NAME}"
    }

    # Build the so
    for i in ${ANDROID_ABI}; do
        CURRENT_ABI=$i
        buildlib
    done
fi

python3 ./../SDK/jplManipulation.py --assemble --plugin=${PLUGIN_NAME} --distribution=${PLATFORM} --extraPath=${EXTRAPATH}
cd ${CONTRIB_PATH}/src/ffmpeg/
# ffmpeg build configuration files were changed during plugin build
# this git checkout will remove these changes
git checkout -- .
