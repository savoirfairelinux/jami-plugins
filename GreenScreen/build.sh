#! /bin/bash
# Build the plugin for the project
set -e
export OSTYPE
ARCH=$(uname -m)
EXTRAPATH=''
# Flags:

# -p: number of processors to use.
# -c: Runtime plugin cpu/gpu setting.
# -t: target platform.
# -d: debug program.

if [ -z "${DAEMON}" ]; then
    DAEMON="./../../daemon"
    echo "DAEMON not provided, building with ${DAEMON}"
fi
if [ -z "${PROCESSOR}" ]; then
    PROCESSOR="CPU"
    echo "PROCESSOR not provided, building with ${PROCESSOR}"
fi

PLUGIN_NAME="GreenScreen"
JPL_FILE_NAME="${PLUGIN_NAME}.jpl"
SO_FILE_NAME="lib${PLUGIN_NAME}.so"
DAEMON_SRC="${DAEMON}/src"
CONTRIB_PATH="${DAEMON}/contrib"
PLUGINS_LIB="../lib"
LIBS_DIR="./../contrib/Libs"
PREFERENCESFILENAME="preferences"
PLATFORM=$(uname)

if [ "${PLATFORM}" = "Linux" ]; then
    PLATFORM="linux-gnu"
    CONTRIB_PLATFORM_CURT=${ARCH}
    echo "Building with ${PLATFORM}"
elif [ "${PLATFORM}" = "Darwin" ]; then
    PLATFORM="darwin"
    SO_FILE_NAME="lib${PLUGIN_NAME}.dylib"
    alias nproc='sysctl -n hw.logicalcpu'
    CONTRIB_PLATFORM_CURT=${ARCH}-apple
    CONTRIB_PLATFORM_EXTRA=$(uname -r)
    echo "Building with ${PLATFORM}"
fi

if [ "${PROCESSOR}" = "CPU" ]; then
    ONNX_LIBS="cpu"
elif [ "${PROCESSOR}" = "NVIDIA" ]; then
    ONNX_LIBS="nvidia-gpu"
    CUBLASLT="-lcublasLt"
    PREFERENCESFILENAME="${PREFERENCESFILENAME}-accel"
fi

while getopts t:c:p:d OPT; do
  case "$OPT" in
    d)
      DEBUG=true
      export __DEBUG__=true
    ;;
    t)
      PLATFORM="${OPTARG}"
      if [ ${PLATFORM} = "android" ]; then
          ONNX_LIBS=""
      fi
    ;;
    c)
    ;;
    p)
    ;;
    \?)
      exit 1
    ;;
  esac
done

cp -r ffmpeg ${CONTRIB_PATH}/src/
cp -r ../contrib/rav1e ${CONTRIB_PATH}/src/

if [ "${PLATFORM}" = "linux-gnu" ] || [ "${PLATFORM}" = "redhat-linux" ]
then
    if [ -f "${CONTRIB_PATH}/native/.ffmpeg" ]; then
        rm "${CONTRIB_PATH}/native/.ffmpeg"
    fi
    WORKPATH=$(pwd)
    cd "${CONTRIB_PATH}/native/"
    make .ffmpeg -j$(nproc)
    rm .ffmpeg
    cd ${WORKPATH}

    CONTRIB_PLATFORM=${CONTRIB_PLATFORM_CURT}-${PLATFORM}
    ONNX_PATH=${EXTRALIBS_PATH}
    if [ -z "${EXTRALIBS_PATH}" ]
    then
      ONNX_PATH="${CONTRIB_PATH}/${CONTRIB_PLATFORM}"
    fi

    if [ ${DEBUG} ]; then
      OUTPUT="${PLUGIN_NAME}"
      CLANG_OPTS="-g -fsanitize=address"
      EXTRA_DEBUG_LIBRARIES="-lyaml-cpp -lvdpau -lX11 -lva-drm -lva-x11 -lrav1e"
      EXTRA_DEFINES="-D__DEBUG__"
    else
      python3 ./../SDK/jplManipulation.py --preassemble --plugin=${PLUGIN_NAME}
      CLANG_OPTS="-O3 -shared"
      OUTPUT="build-local/jpl/lib/${CONTRIB_PLATFORM}/${SO_FILE_NAME}"
    fi

    # Compile
    clang++ -std=c++17 -fPIC ${CLANG_OPTS} \
    -Wl,-Bsymbolic,-rpath,"\${ORIGIN}" \
    -Wall -Wextra \
    -Wno-unused-parameter \
    ${EXTRA_DEFINES} \
    -D${PROCESSOR} \
    -I"." \
    -I"${DAEMON_SRC}" \
    -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/include" \
    -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/include/opencv4" \
    -I"${ONNX_PATH}/include/onnxruntime/session" \
    -I"${ONNX_PATH}/include/onnxruntime/providers/cuda" \
    -I"${PLUGINS_LIB}" \
    ./../lib/common.cpp \
    ./../lib/accel.cpp \
    ./../lib/frameUtils.cpp \
    ./../lib/frameFilter.cpp \
    main.cpp \
    videoSubscriber.cpp \
    pluginMediaHandler.cpp \
    pluginProcessor.cpp \
    -L"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/lib/" \
    -L"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/lib/opencv4/3rdparty/" \
    -L"${ONNX_PATH}/lib/onnxruntime/${ONNX_LIBS}" \
    -L"${CUDA_HOME}/lib64/" \
    -l:libavfilter.a \
    -l:libswscale.a \
    -l:libavformat.a \
    -l:libavcodec.a \
    -l:libavutil.a \
    -l:libvpx.a \
    -l:libx264.a \
    -lopencv_imgproc \
    -lopencv_core \
    -lvpx \
    -lx264 \
    -lspeex \
    -lopus \
    -lz \
    -lva \
    ${CUBLASLT} -lonnxruntime \
    ${EXTRA_DEBUG_LIBRARIES} \
    -o "${OUTPUT}"

    if [ ${DEBUG} ]; then
      cp "${ONNX_PATH}/lib/onnxruntime/${ONNX_LIBS}/libonnxruntime.so" "libonnxruntime.so.1.12.0"
      mkdir -p "./data/model"
      cp "./modelSRC/mModel.onnx" "./data/model/mModel.onnx"
    else
      cp "${ONNX_PATH}/lib/onnxruntime/${ONNX_LIBS}/libonnxruntime.so" "build-local/jpl/lib/$CONTRIB_PLATFORM/libonnxruntime.so.1.12.0"
      mkdir -p "./build-local/jpl/data/model"
      cp "./modelSRC/mModel.onnx" "./build-local/jpl/data/model/mModel.onnx"
      cp "./${PREFERENCESFILENAME}.json" "./build-local/jpl/data/preferences.json"
    fi
    if [ "${PROCESSOR}" = "NVIDIA" ]
    then
      cp "${ONNX_PATH}/lib/onnxruntime/${ONNX_LIBS}/libonnxruntime_providers_shared.so" "build-local/jpl/lib/$CONTRIB_PLATFORM/libonnxruntime_providers_shared.so"
      cp "${ONNX_PATH}/lib/onnxruntime/${ONNX_LIBS}/libonnxruntime_providers_cuda.so" "build-local/jpl/lib/$CONTRIB_PLATFORM/libonnxruntime_providers_cuda.so"
    fi

elif [ "${PLATFORM}" = "darwin" ]
then
    if [ -f "${CONTRIB_PATH}/native/.ffmpeg" ]; then
        rm "${CONTRIB_PATH}/native/.ffmpeg"
    fi
    WORKPATH=$(pwd)
    cd "${CONTRIB_PATH}/native/"
    make .ffmpeg -j$(nproc)
    rm .ffmpeg
    cd ${WORKPATH}

    CONTRIB_PLATFORM=${CONTRIB_PLATFORM_CURT}-${PLATFORM}
    ONNX_PATH=${EXTRALIBS_PATH}
    if [ -z "${EXTRALIBS_PATH}" ]
    then
      ONNX_PATH="${CONTRIB_PATH}/${CONTRIB_PLATFORM}${CONTRIB_PLATFORM_EXTRA}"
    fi

    if [ ${DEBUG} ]; then
      OUTPUT="${PLUGIN_NAME}"
      CLANG_OPTS="-g -fsanitize=address"
      EXTRA_DEBUG_LIBRARIES="-lyaml-cpp -lrav1e"
      EXTRA_DEFINES="-D__DEBUG__"
    else
      python3 ./../SDK/jplManipulation.py --preassemble --plugin=${PLUGIN_NAME}
      CLANG_OPTS="-O3 -shared"
      OUTPUT="build-local/jpl/lib/${CONTRIB_PLATFORM}/${SO_FILE_NAME}"
    fi

    # Compile
    clang++ -std=c++17 -fPIC ${CLANG_OPTS} \
    -Wl,-no_compact_unwind -Wl,-framework,CoreFoundation \
    -Wl,-framework,Security -Wl,-framework,VideoToolbox \
    -Wl,-framework,CoreMedia -Wl,-framework,CoreVideo \
    -Wl,-framework,OpenCl -Wl,-framework,Accelerate \
    -Wl,-rpath,"\${ORIGIN}" \
    -Wall -Wextra \
    -Wno-unused-parameter \
    ${EXTRA_DEFINES} \
    -D${PROCESSOR} \
    -I"." \
    -I"${DAEMON_SRC}" \
    -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}${CONTRIB_PLATFORM_EXTRA}/include" \
    -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}${CONTRIB_PLATFORM_EXTRA}/include/opencv4" \
    -I"${ONNX_PATH}/include/onnxruntime/session" \
    -I"${PLUGINS_LIB}" \
    ./../lib/common.cpp \
    ./../lib/accel.cpp \
    ./../lib/frameUtils.cpp \
    ./../lib/frameFilter.cpp \
    main.cpp \
    videoSubscriber.cpp \
    pluginMediaHandler.cpp \
    pluginProcessor.cpp \
    -L"${CONTRIB_PATH}/${CONTRIB_PLATFORM}${CONTRIB_PLATFORM_EXTRA}/lib/" \
    -L"${CONTRIB_PATH}/${CONTRIB_PLATFORM}${CONTRIB_PLATFORM_EXTRA}/lib/opencv4/3rdparty/" \
    -L"${ONNX_PATH}/lib/onnxruntime/${ONNX_LIBS}" \
    -lavfilter \
    -lswscale \
    -lavformat \
    -lavcodec \
    -lavutil \
    -lvpx -lx264 -lbz2 -liconv -lz \
    -lopencv_imgproc \
    -lopencv_core \
    -lonnxruntime \
    -lspeex \
    -lopus \
    ${EXTRA_DEBUG_LIBRARIES} \
    -o "${OUTPUT}"

    if [ ${DEBUG} ]; then
      mkdir -p "./data/model"
      cp "./modelSRC/mModel.onnx" "./data/model/mModel.onnx"
      cp "${ONNX_PATH}/lib/onnxruntime/${ONNX_LIBS}/libonnxruntime.dylib" "libonnxruntime.dylib"
      install_name_tool -id "@loader_path/libonnxruntime.1.12.0.dylib" "libonnxruntime.dylib"
      install_name_tool -id "@loader_path/${PLUGIN_NAME}" "${OUTPUT}"
    else
      mkdir -p "./build-local/jpl/data/model"
      cp "./modelSRC/mModel.onnx" "./build-local/jpl/data/model/mModel.onnx"
      cp "./${PREFERENCESFILENAME}.json" "./build-local/jpl/data/preferences.json"
      cp "${ONNX_PATH}/lib/onnxruntime/${ONNX_LIBS}/libonnxruntime.dylib" "build-local/jpl/lib/${CONTRIB_PLATFORM}/libonnxruntime.dylib"
      install_name_tool -id "@loader_path/libonnxruntime.1.12.0.dylib" "build-local/jpl/lib/${CONTRIB_PLATFORM}/libonnxruntime.dylib"
      install_name_tool -id "@loader_path/${SO_FILE_NAME}" "${OUTPUT}"
    fi
    install_name_tool -change "@rpath/libonnxruntime.1.12.0.dylib" "@loader_path/libonnxruntime.dylib" "${OUTPUT}"

    if [ -n "${APPLE_SIGN_CERTIFICATE}" ]; then
      codesign --force --verify --timestamp -o runtime --sign "${APPLE_SIGN_CERTIFICATE}"  "build-local/jpl/lib/${CONTRIB_PLATFORM}/libonnxruntime.dylib"
      codesign --force --verify --timestamp -o runtime --sign "${APPLE_SIGN_CERTIFICATE}"  "build-local/jpl/lib/${CONTRIB_PLATFORM}/${SO_FILE_NAME}"
      ditto -c -k --rsrc "build-local/jpl/lib/${CONTRIB_PLATFORM}/libonnxruntime.dylib" "build-local/libonnxruntime.dylib.zip"
      LIBRARYNAME=libonnxruntime.dylib sh ./../notarize.sh
      ditto -x -k "build-local/libonnxruntime.dylib.zip" "build-local/notarized0"
      cp "build-local/notarized0/libonnxruntime.dylib" "build-local/jpl/lib/${CONTRIB_PLATFORM}/libonnxruntime.dylib"

      ditto -c -k --rsrc "build-local/jpl/lib/${CONTRIB_PLATFORM}/${SO_FILE_NAME}" "build-local/${SO_FILE_NAME}.zip"
      LIBRARYNAME=${SO_FILE_NAME} sh ./../notarize.sh
      ditto -x -k "build-local/${SO_FILE_NAME}.zip" "build-local/notarized1"
      cp "build-local/notarized1/${SO_FILE_NAME}" "build-local/jpl/lib/${CONTRIB_PLATFORM}/${SO_FILE_NAME}"
    fi

elif [ "${PLATFORM}" = "android" ]
then
    python3 ./../SDK/jplManipulation.py --preassemble --plugin=${PLUGIN_NAME} --distribution=${PLATFORM}

    if [ -z "$ANDROID_NDK" ]; then
        ANDROID_NDK="/home/${USER}/Android/Sdk/ndk/21.1.6352462"
        echo "ANDROID_NDK not provided, building with ${ANDROID_NDK}"
    fi

    #=========================================================
    #    Check if the ANDROID_ABI was provided
    #    if not, set default
    #=========================================================
    if [ -z "$ANDROID_ABI" ]; then
        ANDROID_ABI="armeabi-v7a arm64-v8a x86_64"
        echo "ANDROID_ABI not provided, building for ${ANDROID_ABI}"
    fi

    buildlib() {
        echo "$CURRENT_ABI"

        #=========================================================
        #    ANDROID TOOLS
        #=========================================================
        export HOST_TAG=linux-x86_64
        export TOOLCHAIN=$ANDROID_NDK/toolchains/llvm/prebuilt/$HOST_TAG
        export AR=$TOOLCHAIN/bin/llvm-ar
        export AS=$TOOLCHAIN/bin/llvm-as
        export LD=$TOOLCHAIN/bin/ld
        export RANLIB=$TOOLCHAIN/bin/llvm-ranlib
        export STRIP=$TOOLCHAIN/bin/llvm-strip
        export ANDROID_SYSROOT=$TOOLCHAIN/sysroot

        if [ "$CURRENT_ABI" = armeabi-v7a ]
        then
        export CC=$TOOLCHAIN/bin/armv7a-linux-androideabi21-clang
        export CXX=$TOOLCHAIN/bin/armv7a-linux-androideabi21-clang++

        elif [ "$CURRENT_ABI" = arm64-v8a ]
        then
        export CC=$TOOLCHAIN/bin/aarch64-linux-android21-clang
        export CXX=$TOOLCHAIN/bin/aarch64-linux-android21-clang++

        elif [ "$CURRENT_ABI" = x86_64 ]
        then
        export CC=$TOOLCHAIN/bin/x86_64-linux-android21-clang
        export CXX=$TOOLCHAIN/bin/x86_64-linux-android21-clang++

        else
        echo "ABI NOT OK" >&2
        exit 1
        fi

        #=========================================================
        #    CONTRIBS
        #=========================================================
        if [ "$CURRENT_ABI" = armeabi-v7a ]
        then
        CONTRIB_PLATFORM=arm-linux-androideabi

        elif [ "$CURRENT_ABI" = arm64-v8a ]
        then
        CONTRIB_PLATFORM=aarch64-linux-android

        elif [ "$CURRENT_ABI" = x86_64 ]
        then
        CONTRIB_PLATFORM=x86_64-linux-android
        fi

        #NDK SOURCES FOR cpufeatures
        NDK_SOURCES=${ANDROID_NDK}/sources/android

        if [ -f "${CONTRIB_PATH}/native-${CONTRIB_PLATFORM}/.ffmpeg" ]; then
            rm "${CONTRIB_PATH}/native-${CONTRIB_PLATFORM}/.ffmpeg"
        fi

        WORKPATH=$(pwd)
        cd "${CONTRIB_PATH}/native-${CONTRIB_PLATFORM}/"
        make .ffmpeg -j$(nproc)
        rm .ffmpeg
        cd ${WORKPATH}

        #=========================================================
        #    Compile CPU FEATURES, NEEDED FOR OPENCV
        #=========================================================
        $CC -c "$NDK_SOURCES/cpufeatures/cpu-features.c" -o cpu-features.o -o cpu-features.o --sysroot=$ANDROID_SYSROOT

        #=========================================================
        #    Compile the plugin
        #=========================================================

        ONNX_PATH="${EXTRALIBS_PATH}/${CURRENT_ABI}"
        if [ -z ${EXTRALIBS_PATH} ]
        then
          ONNX_PATH="${CONTRIB_PATH}/${CONTRIB_PLATFORM}"
        fi

        # Create so destination folder
        $CXX --std=c++17 -O3 -fPIC \
        -Wl,-Bsymbolic,-rpath,"\${ORIGIN}" \
        -shared \
        -Wall -Wextra \
        -Wno-unused-parameter \
        -DANDROID \
        -I"." \
        -I"${DAEMON_SRC}" \
        -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/include" \
        -I"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/include/opencv4" \
        -I"${ONNX_PATH}/include/onnxruntime/session" \
        -I"${ONNX_PATH}/include/onnxruntime/providers/nnapi" \
        -I"${ONNX_PATH}/../include/onnxruntime/session" \
        -I"${ONNX_PATH}/../include/onnxruntime/providers/nnapi" \
        -I"${PLUGINS_LIB}" \
        ./../lib/accel.cpp \
        ./../lib/frameUtils.cpp \
        ./../lib/frameFilter.cpp \
        main.cpp \
        videoSubscriber.cpp \
        pluginMediaHandler.cpp \
        pluginProcessor.cpp \
        cpu-features.o \
        -L"${CONTRIB_PATH}/${CONTRIB_PLATFORM}/lib/" \
        -L"${ONNX_PATH}/lib/" \
        -lavfilter \
        -lswscale \
        -lavformat \
        -lavcodec \
        -lavutil \
        -lvpx \
        -lx264 \
        -lspeex \
        -lopus \
        -liconv \
        -lopencv_imgproc \
        -lopencv_core \
        -llog -lz \
        -lonnxruntime \
        --sysroot=$ANDROID_SYSROOT \
        -o "build-local/jpl/lib/$CURRENT_ABI/${SO_FILE_NAME}"

        rm cpu-features.o
        cp "${ONNX_PATH}/lib/libonnxruntime.so" "build-local/jpl/lib/${CURRENT_ABI}/libonnxruntime.so"
    }

    # Build the so
    for i in ${ANDROID_ABI}; do
        CURRENT_ABI=$i
        buildlib
    done

    mkdir -p "./build-local/jpl/data/model"
    cp "./modelSRC/mModel.ort" "./build-local/jpl/data/model/mModel.ort"
    cp "./${PREFERENCESFILENAME}-accel.json" "./build-local/jpl/data/preferences.json"
fi

if [ ! ${DEBUG} ]; then
  python3 ./../SDK/jplManipulation.py --assemble --plugin=${PLUGIN_NAME} --distribution=${PLATFORM} --extraPath=${ONNX_LIBS}
fi
cd ${CONTRIB_PATH}/src/ffmpeg/
# ffmpeg build configuration files were changed during plugin build
# this git checkout will remove these changes
git checkout -- .
